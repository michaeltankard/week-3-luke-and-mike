﻿using UnityEngine;
using System.Collections;

public class PhysControllerFP : MonoBehaviour 
{
	private Rigidbody move;
	private Vector3 movInputs;
	public Vector3 movePos;
	private Vector3 lookInputs;
	private Quaternion headRot;
	public Transform head;
	public float sinceLastGrounded = 0;
	public float moveSpeed = 10f;
	public float jumpHeight = 15f;
	public float gravity = 15f;
	// Use this for initialization
	void Awake () 
	{
		Screen.lockCursor = true;
		move = this.GetComponents<Rigidbody>()[0];
	}
	
	// Update is called once per frame
	void Update () 
	{
		//pooling inputs
		movInputs.x = Input.GetAxis("Horizontal");
		movInputs.z = Input.GetAxis("Vertical");
		lookInputs.y += Input.GetAxis("Mouse X");
		lookInputs.x -= Input.GetAxis("Mouse Y");

		//detects input for jump, runs jump function
		if(Input.GetButtonDown("Jump") && sinceLastGrounded < 0.1f)
			Jump();

		if(Input.GetButtonDown("Fire1"))
			Screen.lockCursor = true;

		//converting inputs into player Direction
		movePos = new Vector3(head.transform.TransformDirection(movInputs).x  * moveSpeed , movePos.y, head.transform.TransformDirection(movInputs).z * moveSpeed);


		//applying gravity
		if(sinceLastGrounded <= 0.1f)
		{
			movePos.y -= 0.2f * Time.deltaTime;
			movePos.y = Mathf.Clamp(movePos.y, -0.1f, 50f);
			Debug.Log("Grounded");
		}
		else
			movePos.y -= gravity * Time.deltaTime;


		//clamping camera y rotation
		lookInputs.x = Mathf.Clamp(lookInputs.x, -89, 89);

		//converting vector to Quaternion Euler
		headRot = Quaternion.Euler(lookInputs);

		//rotating the camera
		head.rotation = headRot;

		sinceLastGrounded += Time.deltaTime;
	}

	void Jump()
	{
		movePos.y += jumpHeight;
	}

	void FixedUpdate()
	{
		move.velocity = movePos;
	}

	void OnCollisionStay(Collision col)
	{
		foreach (var point in col.contacts)
		{
			if (point.normal.y > 0.2f && point.point.y < collider.bounds.center.y)
				sinceLastGrounded = 0;
		}
		//do stuff
	}

}
