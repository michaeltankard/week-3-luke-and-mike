﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class FBXImporter : AssetPostprocessor
{
	void OnPreprocessModel()
	{
		ModelImporter importer = assetImporter as ModelImporter;
		string path = importer.assetPath.ToLower();
		importer.importMaterials = false;
	}
}
